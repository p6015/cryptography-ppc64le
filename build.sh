#!/bin/bash

git clone https://github.com/pyca/cryptography.git

cd ./cryptography/

git checkout tags/$(git describe --tags --abbrev=0)

python3 -m venv cryptography-venv

pip install --upgrade pip

pip install --editable .

pip3 install --requirement dev-requirements.txt

python3 ./setup.py build

python3 ./setup.py bdist_wheel
